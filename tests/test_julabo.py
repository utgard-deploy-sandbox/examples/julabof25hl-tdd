#!/usr/bin/env python3
# -----------------------------------------------------------------------------
# Jython - CSStudio
# -----------------------------------------------------------------------------
# Julabo F25-HL
# -----------------------------------------------------------------------------
# ESS ERIC - ICS HWI group
# -----------------------------------------------------------------------------
# WP12 - douglas.bezerra.beniz@esss.se
# -----------------------------------------------------------------------------
import pytest
import epics
import time

PREFIX="Julabo"

def test_fake():
    assert 0 == 0

def get_expected_version():
    # Expected version
    return "JULABO FP50_MH Simulator, ISIS"

def test_version():
    # Wait 10 seconds to guarantee the IOC is up and running
    time.sleep(10)
    # EPCIS PV
    pvVersion = ("%s:%s" % (PREFIX, "VERSION"))
    # Reading the value of the PV
    versionValue = epics.caget(pvVersion)
    # Verify test
    assert get_expected_version() == versionValue, "Version should be: %s but received: %s" % (get_expected_version(), versionValue)
