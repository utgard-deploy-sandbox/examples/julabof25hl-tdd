#!/usr/bin/env iocsh.bash

# load modules
require(julabof25hl)
require(streamdevice)

# Set parameters when not using auto deployment
epicsEnvSet(PORTNAME, "PortA")
epicsEnvSet(IPADDR, "$(JULABO_IP)")
epicsEnvSet(IPPORT, "$(JULABO_PORT)")
epicsEnvSet(PREFIX, "$(PREFIX)")
epicsEnvSet(STREAM_PROTOCOL_PATH, "$(julabof25hl_DIR)/db")
epicsEnvSet(TEMPSCAN, "10")
epicsEnvSet(SETPNAME, "SP")
epicsEnvSet(SETPNR, "11")
#epicsEnvSet("LOCATION", "ESTIA; $(IPADDR)")

drvAsynIPPortConfigure("$(PORTNAME)", "$(IPADDR):$(IPPORT)")

#Load your database defining the EPICS records
iocshLoad("$(julabof25hl_DIR)/julabof25hl.iocsh", "P=$(PREFIX), PORT=$(PORTNAME), ADDR=$(IPPORT)")
